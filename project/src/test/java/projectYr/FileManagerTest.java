package projectYr;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FileManagerTest {
	private final String path = ("src/main/resources/test.json");
	FileManager fileManager;
	private final String location = "Fjellet";
	
	private void clearAllLocations() {
		for(String location : fileManager.GetSavedLocations()) {
			fileManager.RemoveLocation(location);
		}
	}
	
	@BeforeEach
	public void setUp() {
		fileManager = new FileManager(path);
		clearAllLocations();
	}
	
	@Test
	@DisplayName("Check that path validation thorugh the constructor validates correctly")
	void FileManagerConstructorTest(){
		String invalidPath1 = ("src/main/resources/test.jsn");
		String invalidPath2 = ("src/main/resources/.json");
		String invalidPath3 = ("src/main/resorces/test.json");
		
		assertThrows(IllegalArgumentException.class, () -> {
			FileManager fileManagerInvalid = new FileManager(invalidPath3);
			}, "The path must be: src/main/resources/");
		
		assertThrows(IllegalArgumentException.class, () -> {
			FileManager fileManagerInvalid = new FileManager(invalidPath2);
			}, "The file itself must be must have a name.");
		
		assertThrows(IllegalArgumentException.class, () -> {
			FileManager fileManagerInvalid = new FileManager(invalidPath1);
			}, "The file format must be .json");
	}
	
	
	@Test
	@DisplayName("Check that nonexistant files are created before, when written to.")
	void createNewFileTest() throws IOException, ParseException {
		JSONObject JSONTestObject = fileManager.ParseStringToJSON("{}");
		String testPath = ("src/main/resources/nonExistant.json");
		File file = new File(testPath);
		file.delete();
		assertTrue(file.createNewFile());
		
		fileManager.addLocation("Fjellet", fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"4\"}"));
		
		

	}
	
	@Test
	@DisplayName("Check that Strings that are not valid JSON throws an error when parsed to an JSONObject")
	void ParseStringToJSONTest() {
		
		assertThrows(org.json.simple.parser.ParseException.class, () -> {
			JSONObject JSONTestObject = fileManager.ParseStringToJSON("\"someCloud\": \"true\"\"fromWind\": \"3\"}");
			}, "This JSON is missing the begining '{' and is invalid");
		
		assertThrows(org.json.simple.parser.ParseException.class, () -> {
			JSONObject JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud true\"\"fromWind\": \"3\"}");
			}, "This JSON is missing the value sperator ':' for the first key.");
		
		assertThrows(org.json.simple.parser.ParseException.class, () -> {
			JSONObject JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"3\"");
			}, "This JSON is missing the ending '}' and is invalid");
	}
	
	@Test
	@DisplayName("Check that locations are correctly added with it's attached data")
	void addLocationTest() throws org.json.simple.parser.ParseException {
		
		JSONObject JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"4\"}");
		
		assertEquals(0, fileManager.GetSavedLocations().size());
		
		fileManager.addLocation(location, JSONTestObject);
		assertEquals(1, fileManager.GetSavedLocations().size());
		assertEquals(location, fileManager.GetSavedLocations().get(0));
		
		assertEquals(JSONTestObject, fileManager.GetJSONFileData().get(location));
	}
	
	@Test
	@DisplayName("Check that unique locations are only added once, and updated if they allready exist")
	void addLocationTest2() throws org.json.simple.parser.ParseException {

		JSONObject JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"4\"}");
		
		assertEquals(0, fileManager.GetSavedLocations().size());
		
		fileManager.addLocation(location, JSONTestObject);
		assertEquals(1, fileManager.GetSavedLocations().size());
		assertEquals(JSONTestObject, fileManager.GetJSONFileData().get(location));
		
		JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"3\"}");
		fileManager.addLocation(location, JSONTestObject);
		assertEquals(1, fileManager.GetSavedLocations().size());
		assertEquals(JSONTestObject, fileManager.GetJSONFileData().get(location));
		
		
	}
	@Test
	@DisplayName("Check that data recieved from file through GetJSONFileData is JSON")
	void  GetJSONFileDataTest() throws org.json.simple.parser.ParseException {

		JSONObject JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"4\"}");

		fileManager.addLocation(location, JSONTestObject);
		
		assertTrue(fileManager.GetJSONFileData()  instanceof JSONObject);
		
	}
	
	@Test
	@DisplayName("Check that a correct list of all locations are retrieved.")
	void  GetSavedLocationsTest() throws org.json.simple.parser.ParseException{
		JSONObject JSONTestObject = fileManager.ParseStringToJSON("{\"someCloud\": \"true\"\"fromWind\": \"4\"}");
		List<String> locations = new ArrayList<>();
		locations.add("1");locations.add("2");locations.add("3");
		
		fileManager.addLocation("1", JSONTestObject);
		fileManager.addLocation("2", JSONTestObject);
		fileManager.addLocation("3", JSONTestObject);
		
		assertEquals(locations, fileManager.GetSavedLocations());
	}
	
	
	@Test
	@DisplayName("Check that removeLocation correctly removes locations")
	void RemoveLocationTest() throws org.json.simple.parser.ParseException {
		List<String> locations = new ArrayList<>();
		locations.add("1");locations.add("2");locations.add("3");
		
		GetSavedLocationsTest();
		assertEquals(locations, fileManager.GetSavedLocations());
		
		fileManager.RemoveLocation("2");
		assertTrue(fileManager.GetSavedLocations().contains("1"));
		assertFalse(fileManager.GetSavedLocations().contains("2"));
		assertTrue(fileManager.GetSavedLocations().contains("3"));
		
		fileManager.RemoveLocation("1");
		assertFalse(fileManager.GetSavedLocations().contains("1"));
		assertFalse(fileManager.GetSavedLocations().contains("2"));
		assertTrue(fileManager.GetSavedLocations().contains("3"));
	}
}
