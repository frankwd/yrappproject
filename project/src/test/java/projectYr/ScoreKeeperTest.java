package projectYr;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;




class ScoreKeeperTest {
	private ScoreKeeper keeper;
	
	@BeforeEach
	public void setUp() {
		this.keeper = new ScoreKeeper();
	}
	
	@Test
	public void testAddMaxScore() {
		keeper.addMaxScore(-10);
		keeper.addMaxScore(20);
		assertEquals(10, keeper.getMaxScore());
	}
	@Test
	public void testRemoveMaxScore() {
		keeper.removeMaxScore(-10);
		keeper.removeMaxScore(20);
		assertEquals(-10, keeper.getMaxScore());
	}
	@Test
	public void testAddScore() {
		keeper.addScore(-10);
		keeper.addScore(20);
		assertEquals(10, keeper.getScore());
	}
	@Test
	public void testRemoveScore() {
		keeper.removeScore(-10);
		keeper.removeScore(20);
		assertEquals(-10, keeper.getScore());
	}
	
	@Test
	public void testGetResult() {
		assertEquals(null, keeper.getResult());
		keeper.addScore(3);
		keeper.addMaxScore(15);
		assertEquals("red", keeper.getResult());
		keeper.addScore(5);
		assertEquals("orange", keeper.getResult());
		keeper.addScore(5);
		assertEquals("green", keeper.getResult());
	}
	
	@Test
	public void testResetScore() {
		keeper.addScore(10);
		keeper.addMaxScore(15);
		keeper.resetScore();
		assertEquals(0, keeper.getScore());
		assertEquals(0, keeper.getMaxScore());
		assertEquals(null, keeper.getResult());
	}
}
