package projectYr;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedHashMap;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class PreferenceSetterTest {
	private final String filePath = ("src/main/resources/preferences.json");
	private FileManagerInterface fileManager = new FileManager(filePath);
	private PreferenceSetter prefSetter;
	private LinkedHashMap<String, String> preferences;
	private final String location = "Fjellet";
	
	@BeforeEach
	public void setUp() {
		prefSetter = new PreferenceSetter();
		this.setUpEmptyPrefValues();
	}
	
	
	@Test
	@DisplayName("Check that locations are trimmed against locations actually added in the app; upon instantiating the preferenceSetter")
	void PreferenceSetterConstructorTester() {
		
		prefSetter.setPreferences(location, preferences);
		
		assertTrue(fileManager.GetSavedLocations().contains(location));
		PreferenceSetter prefSetterTest = new PreferenceSetter();
		assertFalse(fileManager.GetSavedLocations().contains(location));
	}
	
	@Test
	@DisplayName("Check that the user input is validated.")
	void setPreferencesTest1() {
		prefSetter.setPreferences(location, preferences);
		
		
		preferences.put("fromWind",  "-1");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "Winds with negative speed kind of don't make sense when you think about it...");
		
		this.setUpEmptyPrefValues();
		preferences.put("toWind",  "-1");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "Winds with negative speed kind of don't make sense when you think about it...");
		
		this.setUpEmptyPrefValues();
		preferences.put("fromWind",  "6");
		preferences.put("toWind",  "3");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "The \"from wind\" must be less than the chosen \"to wind\"");
		
		this.setUpEmptyPrefValues();
		preferences.put("fromTemp",  "-274");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "I reccomend searching quantumn space if you wish to experience temperatures below abosulute zero.");
		this.setUpEmptyPrefValues();
		preferences.put("fromTemp",  "101");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "You should be the first to live on Mars if you can survive 100 degrees or more..");
		
		
		this.setUpEmptyPrefValues();
		preferences.put("toTemp",  "-274");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "I reccomend searching quantumn space if you wish to experience temperatures below abosulute zero.");
		this.setUpEmptyPrefValues();
		preferences.put("toTemp",  "101");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "You should be the first to live on Mars if you can survive 100 degrees or more..");
		
		this.setUpEmptyPrefValues();
		preferences.put("fromTemp",  "25");
		preferences.put("toTemp",  "22");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "The \"from temp\" must be less than the chosen \"to temp\"");
		
		
		this.setUpEmptyPrefValues();
		preferences.put("fromTime", "-1");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "With minus hours, we can go backwards in time, you have solved Time Traveling! Now i must kill you.");
		this.setUpEmptyPrefValues();
		preferences.put("fromTime", "25");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "I also wish the day had more than 24 hours... I feel ya..");
		
		this.setUpEmptyPrefValues();
		preferences.put("toTime", "-1");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "With minus hours, we can go backwards in time, you have solved Time Traveling! Now i must kill you.");
		this.setUpEmptyPrefValues();
		preferences.put("toTime", "25");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "I also wish the day had more than 24 hours... I feel ya..");
		
		this.setUpEmptyPrefValues();
		preferences.put("fromTime", "13");
		preferences.put("toTime", "10");
		assertThrows(IllegalArgumentException.class, () -> {
			prefSetter.setPreferences(location, preferences);
			}, "The \"from time\" must be less than the chosen \"to time\"");
	}
	
	@Test
	@DisplayName("Check that new locations are correctly added to the preferences file.")
	void setPreferencesTest2() {
		String location2 = "Byen";
		prefSetter.setPreferences(location, preferences);
		
		assertFalse(fileManager.GetSavedLocations().contains(location) && fileManager.GetSavedLocations().contains(location2));
		prefSetter.setPreferences(location2, preferences);
		assertTrue(fileManager.GetSavedLocations().contains(location) && fileManager.GetSavedLocations().contains(location2));
	}
	
	
	@Test
	@DisplayName("Check that the user input is correctly updated and accesible in the preference file.")
	void setPreferencesTest3() {
		

		prefSetter.setPreferences(location, preferences);
		JSONObject localData = (JSONObject) fileManager.GetJSONFileData();
		JSONObject localLocationPreferences = (JSONObject)localData.get(location);
		assertNotEquals("10", localLocationPreferences.get("fromTemp"));
		assertNotEquals("18", localLocationPreferences.get("toTemp"));
		
		preferences.put("fromTemp", "10");
		preferences.put("toTemp", "18");
		prefSetter.setPreferences(location, preferences);
		localData = (JSONObject) fileManager.GetJSONFileData();
		localLocationPreferences = (JSONObject)localData.get(location);
		
		assertEquals("10", localLocationPreferences.get("fromTemp"));
		assertEquals("18", localLocationPreferences.get("toTemp"));
	}
	
	
	
	
	
	
	
	private void setUpEmptyPrefValues() {

			LinkedHashMap<String, String> preferences = new LinkedHashMap<String, String>();
			
			preferences.put("clearSky",  "false");
			preferences.put("someCloud",  "false");
			preferences.put("allCloud",  "false");
			preferences.put("someRain",  "false");
			preferences.put("allRain", "false");
			
			preferences.put("fromWind",  "");
			preferences.put("toWind",  "");
			
			preferences.put("fromTemp",  "");
			preferences.put("toTemp",  "");
			
			preferences.put("fromTime", "");
			preferences.put("toTime", "");
			
			preferences.put("N",  "false");
			preferences.put("NE",  "false");
			preferences.put("E",  "false");
			preferences.put("SE",  "false");
			preferences.put("S",  "false");
			preferences.put("SW",  "false");
			preferences.put("W",  "false");
			preferences.put("NW",  "false");
			
			this.preferences = preferences;
	}
	
	
}
