package projectYr;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;



class WeatherExtractorTest {
	private WeatherExtractor extractor;
	
	@BeforeEach
	public void setUp() {
		extractor = new WeatherExtractor();
	}
	
	
	@Test
	@DisplayName("Check if Add Location input is validated correctly")
	void AddLocationValidationTest() {
		extractor.AddLocation("Trondheim", "63.427", "10.411");
		
		assertThrows(IllegalArgumentException.class, () -> {
			extractor.AddLocation("Trondheim", "63,427", "10.411");
			}, "Invalid latitude coordinates should throw an exception! This uses comma instead of dot.");
		
		assertThrows(IllegalArgumentException.class, () -> {
			extractor.AddLocation("Trondheim", "63.427a", "10.411");
			}, "Invalid latitude coordinates should throw an exception! This has an letter");
		
		assertThrows(IllegalArgumentException.class, () -> {
			extractor.AddLocation("Trondheim", " 63.427", "10.411");
			}, "Invalid latitude coordinates should throw an exception! This has an space");
	
		assertThrows(IllegalArgumentException.class, () -> {
			extractor.AddLocation("", " 63.427", "10.411");
			}, "The location needs a name.");
	}
	
	@Test
	@DisplayName("Check that locations are retrievable as a list")
	void GetSavedLocationTest() {
		String locationName = "Fjellet";
		List<String> locations;
		
		extractor.RemoveLocation(locationName); //make sure it is not in the list.
		locations = extractor.GetSavedLocations();
		extractor.AddLocation(locationName, "41", "41");

		assertEquals(locations.size()+1, extractor.GetSavedLocations().size());
	}
	@Test
	@DisplayName("Check that locations are correctly added and retrievable as a list")
	void AddLocationsTest() {
		String locationName = "Fjellet";
		List<String> locations;
		
		extractor.RemoveLocation(locationName);
		locations = extractor.GetSavedLocations();
		assertFalse(locations.contains(locationName));

		extractor.AddLocation(locationName, "41", "41");
		locations = extractor.GetSavedLocations();
		assertTrue(locations.contains(locationName));
	}

	@Test
	@DisplayName("Check that locations are correctly removed and retrievable as a list")
	void RemoveLocationsTest() {
		String locationName = "Fjellet";
		List<String> locations;
		
		extractor.AddLocation(locationName, "41", "41");
		locations = extractor.GetSavedLocations();
		assertTrue(locations.contains(locationName));
		
		extractor.RemoveLocation(locationName);
		locations = extractor.GetSavedLocations();
		assertFalse(locations.contains(locationName));
	}
	

	@Test
	@DisplayName("Check if the datafile is actually updated and changed, will fail if nothing has changed upstream.")
	void UpdateAllLocationsTest() throws UnknownHostException, IOException {
		JSONObject localJSON = extractor.GetLocalJSON();
		extractor.UpdateAllLocations();
		assertNotEquals(localJSON, extractor.GetLocalJSON());
	}
	
	@Test
	@DisplayName("Check if the datafile is retrievable and valid JSON")
	void GetLocalJSONTest() {
		if(extractor.GetSavedLocations().size() > 0) {
			System.out.println(extractor.GetLocalJSON().size());
			assertTrue(extractor.GetLocalJSON().size() > 0);
		}
		JSONObject data = extractor.GetLocalJSON();
	}
}
