package projectYr;

import java.util.LinkedHashMap;


import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

//import java.time.LocalDateTime;

public class WeatherPresenter {
	private WeatherExtractor extractor;
	
//	Presents data for the given location, to the controller.
//	I am sorry to anyone reading, nesting up the hierarchy of data is just necessarily ugly code.
	public LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> PresentHourlyData(String location) {
		
		extractor = new WeatherExtractor();
		
		LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> hourlyWeatherData = new LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>();
		LinkedHashMap<String, LinkedHashMap<String, String>> dayData = new LinkedHashMap<String, LinkedHashMap<String, String>>();
		LinkedHashMap<String, String> presentableHourData = new LinkedHashMap<String, String>();
		
		JSONObject localJSON = extractor.GetLocalJSON();

		JSONObject locationData = (JSONObject) localJSON.get(location);
		JSONArray timeSeries = (JSONArray) (((JSONObject)locationData.get("properties")).get("timeseries"));
		
		Integer tempDay = 0;
		Integer tempMonth = 0;
		Integer tempYear = 0;
		
		
		for(int i = 0; i< timeSeries.size()-1; i++) {//For 50 hours, not all of them. Error occurs when content of hourDataDataNext_1_hours later changes.
			//Accessing all the data in this one hour
			JSONObject hourData = (JSONObject) timeSeries.get(i);
	        Integer day = Integer.parseInt(((String)hourData.get("time")).substring(8, 10));
	        Integer month = Integer.parseInt(((String)hourData.get("time")).substring(5, 7));
	        Integer year = Integer.parseInt(((String)hourData.get("time")).substring(0, 4));
	        String hour = ((String) hourData.get("time")).substring(11, 16);
	        
	        //Navigating the JSON data structure to access usefull information
	        JSONObject hourDataData = (JSONObject) hourData.get("data");
	        JSONObject Next_1_hours = (JSONObject) hourDataData.get("next_1_hours");
	        if (Next_1_hours == null) 
	        	break;
	        JSONObject Next_1_hoursSymbol = (JSONObject) Next_1_hours.get("summary");
	        JSONObject Next_1_hoursRain = (JSONObject) Next_1_hours.get("details");
	        JSONObject actualData = (JSONObject)(
	        		(JSONObject) hourDataData.get("instant"))
	        		.get("details");
	        
	        //Accessing useful information:
	        String symbol = (String) Next_1_hoursSymbol.get("symbol_code");
	        String precipitation = Double.toString((double) Next_1_hoursRain.get("precipitation_amount"));
	        String temperature = Double.toString((double) actualData.get("air_temperature"));
	        String windSpeed = Double.toString((double) actualData.get("wind_speed"));
	        String windDirection = Double.toString((double) actualData.get("wind_from_direction"));
	        
	        // Storing the useful information, in a presentable manner.
	        presentableHourData.put("time", hour);
	        presentableHourData.put("symbol", symbol);
	        presentableHourData.put("precipitation", precipitation);
	        presentableHourData.put("temperature", temperature);
	        presentableHourData.put("windSpeed", windSpeed);
	        presentableHourData.put("windDirection", windDirection);
	        
	        
	        LinkedHashMap<String, String> thisPresentableHourData = new LinkedHashMap<String,String>(presentableHourData);
	        dayData.put((day+"-"+hour),thisPresentableHourData);
	        
//	        What i am doing here is putting the data for each new day into it's very own "folder". 
//	        The if statement also accounts for the cases of new months and years.
	        if ((day > tempDay && tempDay != 0) || (month > tempMonth && tempMonth != 0) ||(year > tempYear && tempYear != 0)) { 
	        	LinkedHashMap<String, LinkedHashMap<String, String>> todayData = new LinkedHashMap<String, LinkedHashMap<String, String>>(dayData);
	        	String dayKey = ((Integer)(day-1)).toString();
	        	hourlyWeatherData.put(dayKey, todayData);
	        	dayData.clear();
	        }
	        tempYear = year;
	        tempMonth = month;
	        tempDay = day;
		}
	
		
		hourlyWeatherData.put("last day", dayData);
		
		return hourlyWeatherData;
	}
	
	
//    public static void main(String[] args) throws IOException{ 
//    	String location ="Festingen";
////    	String lat_coord = "62.4272";
////    	String long_coord = "11.4122";
//    	
//    	WeatherPresenter presenter = new WeatherPresenter();
//    	
//    	LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> hourlyWeatherData = presenter.PresentHourlyData(location);
//
//    	System.out.println(hourlyWeatherData.get("25").keySet());
//    	System.out.println(hourlyWeatherData.get("26").keySet());
//    	System.out.println(hourlyWeatherData.get("last day").keySet());
//    	System.out.println(hourlyWeatherData.keySet());
//    }
}
