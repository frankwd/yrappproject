package projectYr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class FileManager implements FileManagerInterface{
//	private JSONObject fileData; //The internal state of this object is depreciated for more robust code. It cost in efficiency and may be reimplemented for the extractor
	private String filePath;
	
	public FileManager(String filePath) throws IllegalArgumentException{
		
		this.ValidateFilePath(filePath);
		this.filePath = filePath;
	}

	
	public JSONObject GetJSONFileData() {
		String rawData = ReadFromFile();
		JSONObject localData;
		try {
			localData = ParseStringToJSON(rawData);
			return localData;
		} catch (ParseException e) {
			System.out.println("The data in the file is not valid JSON");
			e.printStackTrace();
			return null;
		}
	}
	
	
	public List<String> GetSavedLocations() {
		List<String> locations = new ArrayList<> (GetJSONFileData().keySet());
		return locations;
	}
	
	
	public void addLocation(String location, JSONObject newData) {
		JSONObject fileData = GetJSONFileData(); //Updates this objects state of fileData to match locally saved file.
		fileData.put(location, newData); //Adds or updates location with the new data.
		WriteToFile(fileData);
		
	}


	public void RemoveLocation(String location)  {
		JSONObject fileData = GetJSONFileData(); //Updates this objects state of fileData to match locally saved file.
		fileData.remove(location);
		WriteToFile(fileData);
	}
	
	
	public JSONObject ParseStringToJSON(String rawData) throws ParseException{
    	JSONParser parser = new JSONParser();
        JSONObject weatherData = (JSONObject) parser.parse(rawData);
        return weatherData;
	}
	
	
//	This method checks if the file exists and creates one if it does not.
	 private void ValidateLocalFileExistence(){
        File file = new File(filePath);
        try {
			if (file.createNewFile()) { //If a new file is being created on this path.
				FileWriter localData = new FileWriter(filePath);
				localData.write("{}");
				localData.close();
			    System.out.println("New JSON file has been created.");
			} 
		} catch (IOException e) {
			System.out.println("The given FilePath cannot be found: "+ filePath);
			e.printStackTrace();
		}
	}

//	Read from file and return content as a String
	private String ReadFromFile() {
		ValidateLocalFileExistence();
	   	 try { // So i open the file with this absolute path, extract the ONE loooong line of JSON and send it out.
	 	    BufferedReader rawData = new BufferedReader(new FileReader(filePath));
	 	    String data = rawData.readLine();
	 	    rawData.close();
	 	    return data;
	 	 }
	   	 catch (IOException e) {
	 	      System.out.println("An error occurred.");
	 	      e.printStackTrace();
	 	      return "Something went wrong reading the file";
	 	  }
	}
	
//	This function simply takes this Objects current state and writes it's content to the file. This function OVERWRITES the existing file.
	private void WriteToFile(JSONObject fileData) {
		ValidateLocalFileExistence();
		try {
			FileWriter localData = new FileWriter(filePath);
			localData.write(fileData.toJSONString());
			localData.flush();
			localData.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	private void ValidateFilePath(String path) throws IllegalArgumentException {
		String regEx1 = ("src\\/main\\/resources\\/.*");
		String regEx2 = ("src\\/main\\/resources\\/[a-z,A-Z].+");
		String regEx3 = ("src\\/main\\/resources\\/[a-z,A-Z]+.json");
		
		if(!path.matches(regEx1))
			throw new IllegalArgumentException("The chosen file path is invalid.");
		if(!path.matches(regEx2))
			throw new IllegalArgumentException("The chosen file name is invalid");
		if(!path.matches(regEx3))
			throw new IllegalArgumentException("The chosen file format is invalid");
	}
}
