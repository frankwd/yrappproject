package projectYr;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONObject;

public class PreferenceSetter {
	private final String filePath = ("src/main/resources/preferences.json");
	private FileManagerInterface fileManager;
	private WeatherExtractor weatherExtractor;
	
	public PreferenceSetter() throws IllegalArgumentException {

		fileManager= new FileManager(filePath);


		trimPreferenceLocations(); //It's ok that this only runs once , once preferences are changed
	}
	
	
//	This method receives preferences for a given location and stores to file.
	public void setPreferences(String location, LinkedHashMap<String, String> preferences) throws IllegalArgumentException{
		JSONObject newPreferences = new JSONObject(preferences); //Maybe i can skip this step and put the HashMap directly into the JSONObject. verify at later stage.
		validateUserInputs(newPreferences);
		fileManager.addLocation(location, newPreferences);
	}
	
	
//	Makes sure the locations stored with preferences are always a subset of locations currently in the apps view.
	private void trimPreferenceLocations() {
		weatherExtractor = new WeatherExtractor();
		List<String> locations = weatherExtractor.GetSavedLocations();
		List<String> preferenceLocations = fileManager.GetSavedLocations();
		for(String location:preferenceLocations) {
			if (!locations.contains(location)) {
				fileManager.RemoveLocation(location);
			}
		}
	}
	
	
	private void validateUserInputs(JSONObject preferences) throws IllegalArgumentException{
		List<String> preferenceTypes = Arrays.asList("fromTime", "toTime", "fromWind", "toWind", "fromTemp", "toTemp");
		
		String isNumberRegEx = ("-?[0-9]*");
		for(String preference : preferenceTypes) {
			
			String value = (String) preferences.get(preference);
			if (!value.matches(isNumberRegEx)) {
				throw new IllegalArgumentException("It's all a numbers game, you typed "+value+" in "+preference);
			}
			
			if (!value.isEmpty()) { //Empty fields must definitely be valid.
				if(preference.equals("fromTime") || preference.equals("toTime")) {
					Integer timeValue = Integer.parseInt(value);
					if (timeValue < 0) {
						throw new IllegalArgumentException("With minus hours, we can go backwards in time, you have solved Time Traveling! Now i must kill you.");
					}
					if (timeValue > 24) {
						throw new IllegalArgumentException("I also wish the day had more than 24 hours... I feel ya..");
					}
					if(preference.equals("fromTime")) {
						String toTime = (String) preferences.get("toTime");
						if(!toTime.isEmpty()) {
							Integer toTimeValue = Integer.parseInt(toTime);
							if(toTimeValue < timeValue)
								throw new IllegalArgumentException("The \"from time\" must be less than the chosen \"to time\"");
						}
					}
				}
				
				if (preference.equals("fromWind") || preference.equals("toWind")) {
					Integer windValue = Integer.parseInt(value);
					if (windValue < 0) {
						throw new IllegalArgumentException("Winds with negative speed kind of makes sense when you think about it...");
					}
					if(preference.equals("fromWind")) {
						String toWind = (String) preferences.get("toWind");
						if(!toWind.isEmpty()) {
							Integer toWindValue = Integer.parseInt(toWind);
							if(toWindValue < windValue)
								throw new IllegalArgumentException("The \"from Wind\" must be less than the chosen \"to Wind\"");
						}
					}
				}
				
				if ((preference.equals("fromTemp") || preference.equals("toTemp"))) {
					
					Integer tempValue = Integer.parseInt(value);
					if (tempValue < -273) {
						throw new IllegalArgumentException("I reccomend searching quantumn space if you wish to experience temperatures below abosulute zero.");
					}
					if (tempValue > 100) {
						throw new IllegalArgumentException("You should be the first to live on Mars if you can survive 100 degrees or more..");
					}
					if(preference.equals("fromTemp")) {
						String toTemp = (String) preferences.get("toTemp");
						if(!toTemp.isEmpty()) {
							Integer toTempValue = Integer.parseInt(toTemp);
							if(toTempValue < tempValue)
								throw new IllegalArgumentException("The \"from Temp\" must be less than the chosen \"to Temp\"");
						}
					}
				}
			}

		}
	}
	
//	private void resetAllPreferences() {
//	PreferenceVerifier verifier = new PreferenceVerifier();
//	WeatherExtractor extractor = new WeatherExtractor();
//	
//	for(String location : extractor.fileManager.GetSavedLocations()) {
//		if ((verifier.fileManager.GetSavedLocations()).contains(location)) {
//			verifier.fileManager.RemoveLocation(location);
//		}
//	}
//}
}
