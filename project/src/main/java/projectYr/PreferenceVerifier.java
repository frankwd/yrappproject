package projectYr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONObject;


public class PreferenceVerifier {
	private final String filePath = ("src/main/resources/preferences.json");
	private FileManagerInterface fileManager;
	private WeatherPresenter presenter = new WeatherPresenter();
	private ScoreKeeper scoreKeeper= new ScoreKeeper();
	
	
	PreferenceVerifier() throws IllegalArgumentException{
			fileManager= new FileManager(filePath);
	}
//	This method returns a value based on the preferences of the given location and tomorrows weather..
	public String getResults(String location) {
		scoreKeeper.resetScore();
		if(!fileManager.GetSavedLocations().contains(location)) {
			return null;
		}
		preferenceDataComparator(location);
//		System.out.println(location);
//		System.out.print("max: " + scoreKeeper.getMaxScore());
//		System.out.println(", actual: " + scoreKeeper.getScore());
		return scoreKeeper.getResult();
	}
	
	
	public JSONObject getJSONFileData() {
		return fileManager.GetJSONFileData();
	}
	
	// This method gets the actual weather data and the preferences, and compares them.
	// The actual comparing takes place for every hour, in four separate "washing boards"
	// The logic has evolved as a result of my expected "result/colour" and not on analytical work. So the following functions and value system is an absolute mess.
	// I value correct wind conditions the most, as a kiter this is how i will use the app. Certain clear sky days are also highly rewarded.
	private void preferenceDataComparator(String location) {
		LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> hourlyWeatherData = presenter.PresentHourlyData(location);
		String tomorrowKey = (String) hourlyWeatherData.keySet().toArray()[1];
		LinkedHashMap<String, LinkedHashMap<String, String>> hourlyDataTomorrow = hourlyWeatherData.get(tomorrowKey);
		
		JSONObject preferences = (JSONObject) fileManager.GetJSONFileData().get(location);
		Integer fromTime = 0;
		Integer toTime = 24;
		if(!preferences.get("fromTime").toString().isEmpty()) 
			fromTime = Integer.parseInt(preferences.get("fromTime").toString());

			
		if(!preferences.get("toTime").toString().isEmpty())
			toTime = Integer.parseInt(preferences.get("toTime").toString());
		
		
		for(String hour : hourlyDataTomorrow.keySet()) {
			LinkedHashMap<String, String> data = new LinkedHashMap<String, String>(hourlyDataTomorrow.get(hour));
			Integer time = Integer.parseInt(data.get("time").substring(0,2));
			
			
			if (time >= fromTime && time <=toTime) {
				calculateWeatherScore(data, preferences);
				calculateWindSpeedScore(data, preferences);
				calculateWindDirectionScore(data, preferences);
				calculateTemperatureScore(data, preferences);
				}
			}
		}
	
	
	private void calculateWeatherScore(LinkedHashMap<String, String> data, JSONObject preferences) {
		Double actualRain = Double.parseDouble(data.get("precipitation"));
		String actualSymbol = data.get("symbol");
		List<String> possibleWeatherChoices = Arrays.asList("clearSky", "someCloud", "allCloud", "someRain", "allRain");
		
		List<String> chosenWeather = new ArrayList<>();
		for (String weather : possibleWeatherChoices) {
			if (Boolean.parseBoolean((String) preferences.get(weather)))  //If checked by user, it will be true.
				chosenWeather.add(weather);
		}
		if (chosenWeather.size()==0)
			return;
		
		if(actualRain > 0) {
			if (!chosenWeather.contains("someRain") && !chosenWeather.contains("allRain")) {
				scoreKeeper.addMaxScore(10);
				if(chosenWeather.contains("clearSky") || chosenWeather.contains("someCloud"))
					scoreKeeper.removeScore(5);
				return;
			}
			else if (chosenWeather.contains("someRain")) {
				if (actualRain <= 1) {
					scoreKeeper.addScore(10);
					scoreKeeper.addMaxScore(10);
				}
			}
			else if (chosenWeather.contains("allRain")) {
				if (actualRain > 1) {
					scoreKeeper.addScore(10);
					scoreKeeper.addMaxScore(10);
					}
				}
			}
		
		List<String> clearSkySymbols = Arrays.asList("clearsky_day", "clearsky_night" );
		List<String> someCloudSymbols = Arrays.asList( "fair_day",  "fair_night", "partlycloudy_day", "partlycloudy_night");
		
		if(chosenWeather.contains("clearSky") && clearSkySymbols.contains(actualSymbol)) {
			scoreKeeper.addScore(40);
			scoreKeeper.addMaxScore(20);
		}
		else if(chosenWeather.contains("someCloud") && someCloudSymbols.contains(actualSymbol)) {
			scoreKeeper.addScore(10);
			
		}

		if(chosenWeather.size() == 1 && chosenWeather.get(0).toString().contains("clearSky")) {
			if(!actualSymbol.contains("clearsky_day") && !actualSymbol.contains("fair_day")) {
				scoreKeeper.removeScore(10);
			}
		}
		if(chosenWeather.size() == 2 && chosenWeather.get(0).toString().contains("clearSky") && chosenWeather.get(0).toString().contains("clearSky")) {
			if(!actualSymbol.contains("clearsky_day") || !actualSymbol.contains("fair_day")) {
				scoreKeeper.removeScore(7);
			}
		}
				
			
		
	}
		
	
	
	
	
	private void calculateWindDirectionScore(LinkedHashMap<String, String> data, JSONObject preferences) {
		List<String> possibleWindDirections = Arrays.asList("NE", "E", "SE", "S", "SW", "W", "NW", "N");
		String ActualWindDirection = null;
		Integer actualDegree =(int) Double.parseDouble(data.get("windDirection"));
		
		//Calculate the actual wind direction of this particular hour in time.
		for (int i = 0; i < possibleWindDirections.size(); i++) {
			int minDeg = 22 + 45*i;
			int maxDeg = 67 + 45*i;
			if (maxDeg > 360) {//In the case of North
				ActualWindDirection = possibleWindDirections.get(i);
				break;
			}
			if (actualDegree > minDeg && actualDegree <= maxDeg) {
				ActualWindDirection = possibleWindDirections.get(i);
				break;
				}
			}
		//Extract the directions from preferences on this particular location
		List<String> chosenWindDirections = new ArrayList<>();
		for (String direction : possibleWindDirections) {
			if (Boolean.parseBoolean((String) preferences.get(direction)))  //If checked by user, it will be true.
				chosenWindDirections.add(direction);
		}
		if (chosenWindDirections.size() == 0)
			return;
		
		if (chosenWindDirections.contains(ActualWindDirection)) {
			scoreKeeper.addScore(20);
			scoreKeeper.addMaxScore(20);
		}
		else
			scoreKeeper.addMaxScore(20);
	}
	
	
	
	private void calculateWindSpeedScore(LinkedHashMap<String, String> data, JSONObject preferences) {
		Double actualWindSpeed = Double.parseDouble(data.get("windSpeed"));
		String fromWindSpeed = preferences.get("fromWind").toString();
		String toWindSpeed= preferences.get("toWind").toString();
		
		calculateWindOrTemperatureScore(actualWindSpeed,fromWindSpeed,toWindSpeed, 5);
	}
	private void calculateTemperatureScore(LinkedHashMap<String, String> data, JSONObject preferences) {
		Double actualTemperature = Double.parseDouble(data.get("temperature"));
		String fromTemp = preferences.get("fromTemp").toString();
		String toTemp= preferences.get("toTemp").toString();
		
		calculateWindOrTemperatureScore(actualTemperature,fromTemp,toTemp,0);
	}
	
	
	private void calculateWindOrTemperatureScore(Double actualValue, String fromValue, String toValue, int windBonus) {
		if (fromValue.isEmpty() && toValue.isEmpty()) 
			return;
		else if (fromValue.isEmpty() && !toValue.isEmpty()) {
			Double toTempVal = Double.parseDouble(toValue);
			if (toTempVal > actualValue) {
				scoreKeeper.addScore(10 + windBonus);
				scoreKeeper.addMaxScore(10);
			}
			else
				scoreKeeper.addMaxScore(10);
		}
		else if (!fromValue.isEmpty() && toValue.isEmpty()) {
			Double fromTempVal = Double.parseDouble(fromValue);
			if (fromTempVal < actualValue) {
				scoreKeeper.addScore(10 + windBonus);
				scoreKeeper.addMaxScore(10);
			}
			else
				scoreKeeper.addMaxScore(10);
		}
		else if (!fromValue.isEmpty() && !toValue.isEmpty()) {
			Double fromTempVal = Double.parseDouble(fromValue);
			Double toTempVal = Double.parseDouble(toValue);
			if (fromTempVal < actualValue && toTempVal > actualValue) {
				scoreKeeper.addScore(10 + windBonus);
				scoreKeeper.addMaxScore(10);
			}
			else
				scoreKeeper.addMaxScore(10);
		}
	}

	

//  public static void main(String[] args) throws IOException{ 
//	PreferenceVerifier verifier = new PreferenceVerifier();
//	WeatherExtractor extractor = new WeatherExtractor();
//
//	
//	for(String location : extractor.fileManager.GetSavedLocations()) {
//		if ((verifier.fileManager.GetSavedLocations()).contains(location))
//			System.out.println(location);
//			System.out.println(verifier.getResults(location));
//	
//	
//		verifier.resetAllPreferences();
//	}
	
	
	

	
	
}
	
	
	
	
	
	
	
	
	

