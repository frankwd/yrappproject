package projectYr;

import java.util.List;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

// I might have wanted to make both a statefull/stateless FileManager. However, for this i would want to use an Abstract Class, and i must use Interface.

public interface FileManagerInterface {
	
	public JSONObject GetJSONFileData();
	
	public List<String> GetSavedLocations();
	
	public void addLocation(String location, JSONObject newData);

	public void RemoveLocation(String location);
	
	public JSONObject ParseStringToJSON(String rawData) throws ParseException ;
}


