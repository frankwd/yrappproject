package projectYr;

//For writing and reading from files.
import java.io.BufferedReader;
import java.io.IOException;

// For getting info from url
import java.net.URL;
import java.net.UnknownHostException;
import java.net.HttpURLConnection;
import java.io.InputStreamReader;


//For parsing JSON
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

//For exporting list of added locations
import java.util.List;


//For deciding how old my local data is
import java.time.LocalDateTime;
import java.time.Duration;


public class WeatherExtractor {
	private final String filePath = ("src/main/resources/weatherData.json");
	private FileManagerInterface fileManager;
	
	WeatherExtractor() throws IllegalArgumentException{

		fileManager= new FileManager(filePath);

	}
	
	public void AddLocation(String location, String lat_coord, String long_coord) throws IllegalArgumentException{
		String lat_coordRegex = ("-?[0-9]{1,2}(\\.[0-9]+)?");
		String long_coordRegex = ("-?[0-9]{1,3}(\\.[0-9]+)?");
		String lat_coordRegexLength = ("-?[0-9]{1,2}(\\.[0-9]{0,4})?");
		String long_coordRegexLength = ("-?[0-9]{1,3}(\\.[0-9]{0,4})?");
		
		if (location.length() == 0) {
			throw new IllegalArgumentException("The location needs a name");
		}
		else if (!lat_coord.matches(lat_coordRegex)) {
			throw new IllegalArgumentException(lat_coord + ": Is not a valid coordinate");
		}
		else if (!long_coord.matches(long_coordRegex)) {
			throw new IllegalArgumentException(long_coord + ": Is not a valid coordinate");
		}
		else if (Double.parseDouble(long_coord) > 180 || Double.parseDouble(long_coord) < -180)
			throw new IllegalArgumentException(long_coord + ": Needs to be +/- 180 degrees");
		
		else if (Double.parseDouble(lat_coord) > 90 || Double.parseDouble(lat_coord) < -90)
			throw new IllegalArgumentException(lat_coord + ": Needs to be +/- 90 degrees");
		
		while (!lat_coord.matches(lat_coordRegexLength)) {
			lat_coord = lat_coord.substring(0, lat_coord.length()-1);
		}
		while (!long_coord.matches(long_coordRegexLength)) {
			long_coord = long_coord.substring(0, long_coord.length()-1);
		}
		
		List<String> locations = this.GetSavedLocations();
		if (!locations.contains(location)) {
			System.out.println(String.format("Location %s added", location));
			JSONObject APIData;
			try {
				APIData = this.GetLocationDataFromAPI(lat_coord, long_coord);
				fileManager.addLocation(location, APIData);
			} catch (IOException e) { 
				System.out.println("Invalid file Path");
//				e.printStackTrace();
			}
			
		}
		else System.out.println("Location already exists");
	}
	
// This method removes a location, if it exists.
	public void RemoveLocation(String location) {
		fileManager.RemoveLocation(location);
	}
	
	
//	Returns a list of the locations added by the user.
	public List<String> GetSavedLocations() {
		return fileManager.GetSavedLocations();
	}
	
//	Updates all locations if they are out-dated.
	public void UpdateAllLocations() throws UnknownHostException, IOException{
		List<String> locations = this.GetSavedLocations();
		
		for (String location : locations) {
			this.UpdateLocalLocationData(location);
		}
		
		

		
	}
	
	public JSONObject GetLocalJSON() {
		return fileManager.GetJSONFileData();
	}
		
//	This function is necessarily somewhat complex, requests data from yr API with an https: request.
	private JSONObject GetLocationDataFromAPI(String lat_coord, String long_coord) throws IOException { //This method requests data from http API and returns it in the form of a STRING
		
		String LocationURL = (String.format("https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=%s&lon=%s", lat_coord,long_coord));
		URL locationUrl = new URL(LocationURL);
		System.out.print(LocationURL+" ");
		HttpURLConnection yrConnection = (HttpURLConnection) locationUrl.openConnection();
		yrConnection.setRequestMethod("GET");
		yrConnection.setRequestProperty("User-Agent", "NTNU Informatikk Student project, Frank W. Daniels, Trondheim <- S�KER SOMMERJOBB :D -> Frankwd@stud.ntnu.no - Send an email and request access to the project.");
		int status = yrConnection.getResponseCode();
		if (status != 200) throw new RuntimeException("HttpResponseCode: " + status);
		
//		Get the expiration time, and then everything else from the YR-Connection.
		long timeExpires = yrConnection.getExpiration();
		BufferedReader connectionData = 
				new BufferedReader(
				new InputStreamReader(
						yrConnection.getInputStream()));
		String rawLocationData = connectionData.readLine();
		yrConnection.disconnect();
		
		//Before returning the API-Data, parse it to JSON and add the "expires" value of this data.
		JSONObject locationData;
		try {
			locationData = fileManager.ParseStringToJSON(rawLocationData);
			locationData.put("expires", timeExpires);
			return locationData;
		} catch (ParseException e) {
			System.out.println("The data recieved from the YR-API is not valid JSON");
			System.out.println("char position of exception: " + e.getPosition());
			e.printStackTrace();
			return null;
		}
		
	}

//	 This method finds the "location" in the JSON data, deletes all children of this location
//	 and writes new data to specific location and writes all data back to the file.
	private void UpdateLocalLocationData(String location) throws IOException, UnknownHostException{ 

		JSONObject locationData = (JSONObject)fileManager.GetJSONFileData().get(location);
		JSONArray coordinates = (JSONArray)((JSONObject)locationData.get("geometry")).get("coordinates");
		String lat_coord = (String)coordinates.get(1).toString();
		String long_coord = (String)coordinates.get(0).toString();
		
		long timeExpires = (long)locationData.get("expires");
		long timeNow = System.currentTimeMillis();
		
		if (timeExpires<timeNow) {
			JSONObject APIData = GetLocationDataFromAPI(lat_coord, long_coord);
			fileManager.addLocation(location, APIData);


		}
		else
			System.out.print("Allready up to date! ");
	}
	

//	=======================================================================
//	FUNCTIONS BELOW STILL NOT PART OF FINAL APPLICATION ----------------------------->
//	=======================================================================
	

	//This functions purpose has changed to simply show "Updated xx:yy hours ago" in the GUI as GET(if expired) exists.
//	private long GetDataAge(String location) throws IOException {
//		JSONObject locationData = (JSONObject) fileManager.GetJSONFileData().get(location);
////		TODo parse locationData to the children of chosen location before sending down.
//		String lastUpdated = GetLocationLastUpdated(locationData);
//		System.out.println(lastUpdated);
//		
//		LocalDateTime lastUpdate = LocalDateTime.parse(lastUpdated.subSequence(0, lastUpdated.length()-1));
//		System.out.println(LocalDateTime.now());
//		Duration offset = Duration.between(lastUpdate, LocalDateTime.now());
//
//		return offset.toSeconds(); 
//	}
//	
//	
////	Takes in the children of a locally stores location as a JSONObject, returns the "last updated" variable.
//	private String GetLocationLastUpdated(JSONObject locationData) {
//		String lastUpdated = (String) ((JSONObject) (((JSONObject)locationData
//				.get("properties"))
//				.get("meta")))
//				.get("updated_at");
//		return lastUpdated;
//	}

	
	
//    public static void main(String[] args) throws IOException, ParseException{ 
//    	String location ="Et sted";
//    	String lat_coord = "63.4271";
//    	String long_coord = "10.4129";
//
//    	WeatherExtractor extractor = new WeatherExtractor();
//
//    	extractor.UpdateAllLocations();
//    }
}
















