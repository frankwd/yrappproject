package projectYr;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

//The Preference verifier could potentially be replaced by a comparable weatherData class. That can be instnziated by both the preferences or actual weather and use the compare interface to decide the match or mismatch.
// Or a by making a weather comparator, since the weather can and will be compared in multiple ways.

// TODO list: possible improvements for the application:
// (1): A small calculator that takes in a wind speed and a kite size and returns the resultant power for comparison.
// (2): I would love it if i could add locations through the google maps API, as well as view existing locations as points on the same map.
// 3: Order the locations alphabetically
// 4: Add the periodic forecast
// 5: Balance the preference score functions to reflect expected results.
// (6): Implement weather history functionality.
// 7: Hours should show from 01, not 00. And i also have to do a switch: make 00 show as 01, 01 as 02, 02 as 03, to show the forecast as yr.no does. Thinking about it this could very well be easier than expected. As i allready have 00 there, and want to remove it :D
// (8): The location buttons could be rendered to also show meters above sea level as this comes free with the weather data.

	public class YrAppMain extends Application{

		@Override
		public void start(Stage stage) throws Exception {
			Parent root = FXMLLoader.load(getClass().getResource("YrAppGUI.fxml"));
			stage.setTitle("Yr-App");
			
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("stylish.css").toExternalForm());
			stage.setScene(scene);
			
			stage.show();
		}

		public static void main(String[] args) {
			
			launch(args);
			
		}
		

			
	}
