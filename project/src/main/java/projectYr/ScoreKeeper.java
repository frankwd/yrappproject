package projectYr;

public class ScoreKeeper {
	private int score;
	private int maxScore;
	
	
	public int getScore() {
		return score;
	}
	public int getMaxScore() {
		return maxScore;
	}
	public void addScore(int score) {
		this.score += score;
	}
	public void addMaxScore(int maxScore) {
		this.maxScore += maxScore;
	}
	public void removeMaxScore(int maxScore) {
		this.maxScore -= maxScore;
	}
	public void removeScore(int score) {
		this.score -= score;
	}
	
	public void resetScore() {
		score = 0;
		maxScore = 0;
	}
	public String getResult(){
		Double score = (double) this.score;
		Double maxScore = (double) this.maxScore;
		
		if (score == 0 && maxScore == 0) 
			return null;
		
		Double percent = (score/maxScore)*100;
		
		if (percent >= 75.0) 
			return "green";
		
		else if (percent < 75 && percent >= 33)
			return "orange";
		
		else
			return "red";
	}
}
