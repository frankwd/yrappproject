package projectYr;

import java.awt.image.BufferedImage;
//For importing images to GUI
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.UnknownHostException;
//For reading information to display.
import java.util.LinkedHashMap;
import java.util.List;

//For the initialize function.
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import org.json.simple.JSONObject;

//For displaying date on GUI
import java.time.LocalDateTime;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.layout.HBox;



public class YrAppController implements Initializable{
	private WeatherExtractor weatherExtractor;
	private PreferenceSetter preferenceSetter;
	private PreferenceVerifier preferenceVerifier;
	private WeatherPresenter presenter;
	private String location;
	private int checkoutDay = 0;
	
	@FXML
	private VBox LocationContainer;
	@FXML
	private VBox ForecastDisplay;
	@FXML
	private Button removeThisLocationButton;
	@FXML
	private Button previousDayButton;
	@FXML
	private Button nextDayButton;
	@FXML
	private Button addLocationButton;
	@FXML
	private MenuButton AddPreferenceButton;
	@FXML
	private TextField locationName;
	@FXML
	private TextField locationLatitude;
	@FXML
	private TextField locationLongitude;
	@FXML 
	private ToggleButton toggleButton;
	@FXML 
	private Text checkoutDateText;
	@FXML 
	private Text preferenceLabel;
	@FXML
	private TextArea addLocationErrorMessage;
	@FXML
	private TextArea addPreferenceErrorMessage;
	@FXML
	private CustomMenuItem AddPreferenceMenu;
	@FXML
	private CustomMenuItem addLocationMenu;
	@FXML
	private CheckBox clearSky;
	@FXML
	private CheckBox someCloud;
	@FXML
	private CheckBox allCloud;
	@FXML
	private CheckBox someRain;
	@FXML
	private CheckBox allRain;
	
	@FXML
	private TextField fromWind;
	@FXML
	private TextField toWind;
	
	@FXML
	private TextField fromTemp;
	@FXML
	private TextField toTemp;
	
	@FXML
	private TextField fromTime;
	@FXML
	private TextField toTime;
	
	@FXML
	private CheckBox N;
	@FXML
	private CheckBox NE;
	@FXML
	private CheckBox E;
	@FXML
	private CheckBox SE;
	@FXML
	private CheckBox S;
	@FXML
	private CheckBox SW;
	@FXML
	private CheckBox W;
	@FXML
	private CheckBox NW;
	@FXML
	private MenuButton addLocationMenuButton;
	
	
	
	
	
	
	
	
	
	@Override
	public void initialize(URL systemLocation, ResourceBundle resources) {
		LoadBigSun();
		SetDateText();
		try {
			weatherExtractor= new WeatherExtractor();
			preferenceSetter = new PreferenceSetter();
			preferenceVerifier = new PreferenceVerifier();
			LoadLocationsToGUI(); //Done last in case of error displays
		}catch (IllegalArgumentException err){
			DisplayInformationText("Could not open file", err.getMessage(), "red");
		}
	}
	
	
	private void LoadLocationsToGUI() {
			try {
				this.weatherExtractor.UpdateAllLocations();
			}
			catch (UnknownHostException e1) {
				System.out.println("Check your internet!");
				DisplayInformationText("Could not update!","Check your internet connection!", "red");
			}
			catch (IOException e2) {
				DisplayInformationText("Error reading file!","","red");
			}

		if (LocationContainer.getChildren() != null) LocationContainer.getChildren().clear();
		
		List<String> locations = weatherExtractor.GetSavedLocations();
		locations.forEach(location -> {

			String result = preferenceVerifier.getResults(location);
			Button locationButton = new Button();
			
			if(result != null) {
				locationButton.setStyle(String.format("-fx-text-fill: %s;", result));
			}
			
			
			locationButton.setText(location);
			locationButton.setMaxWidth(270);
			locationButton.setOnAction(doThis ->{this.location = location; try {LocationClicked(location);} catch (IOException e) {e.printStackTrace();}});
			LocationContainer.getChildren().add(locationButton);
			});

//		LocationContainer.setFillWidth(true);
	}
	
	private void LocationClicked(String location) throws IOException {
		this.nextDayButton.setDisable(false);
		this.AddPreferenceButton.setDisable(false);
		if(toggleButton.isSelected() == false) {
			DisplayHourlyWeatherData(location);
		}
		else DisplayPeriodicWeatherData(location);
		
		removeThisLocationButton.setText("Remove "+location);
		removeThisLocationButton.setDisable(false);
	}
	


	
	private void LoadBigSun() {
		ForecastDisplay.getChildren().clear();
		this.nextDayButton.setDisable(true);
		this.previousDayButton.setDisable(true);
		
//		FileInputStream input;
		try {
//			String path = ("src/main/resources/icons/fair_day.PNG");
//			input = new FileInputStream(path);
//			Image image = new Image(input);



//			ClassLoader classLoader = ClassLoader.getSystemClassLoader();
//	        URL resource = classLoader.getResource("/fair_day.PNG");
//	        InputStream input = resource.openStream();

			

	        
	        
			InputStream input = YrAppController.class.getResourceAsStream("/fair_day.PNG");
            Image image = new Image(input);

			ImageView symbol = new ImageView(image);
			symbol.setFitHeight(300);
			symbol.setFitWidth(300);
			
			ForecastDisplay.getChildren().add(symbol);
			ForecastDisplay.setPadding(new Insets(40,10,10,160));
			} 
//		catch (FileNotFoundException e) {
//			e.printStackTrace();
//			System.out.println("YOOOOOOOOO CANT FIND SHIT ::::::______________________________________________________________________________");
//		}
//		catch (IOException e) {
//			System.out.println("YOOOOOOOOO THIS DOES NOT WORK ::::::______________________________________________________________________________");
//			e.printStackTrace();
//		}
		catch (IllegalArgumentException e) {
			System.out.println("YOOOOOOOOO THIS IS ILLEGAL ::::::______________________________________________________________________________");
			e.printStackTrace();}
	}

	private void DisplayInformationText(String info, String additionalInfo, String colour) {
		this.LoadBigSun();
		
		Label text = new Label();
		text.setText(info);
		text.setPadding(new Insets(-640,0,0,-20));
		text.setStyle(String.format("-fx-text-fill: %s;", colour));
		
		Label additionalText = new Label();
		additionalText.setText(additionalInfo);
		additionalText.setPadding(new Insets(-40,0,0,-100));
		
		ForecastDisplay.getChildren().add(text);
		ForecastDisplay.getChildren().add(additionalText);
	}
	


//	This function displays all available hours of the given day
	private void DisplayHourlyWeatherData(String location) throws IOException {
		this.location = location; //Setting the window state.
		
		ForecastDisplay.getChildren().clear();
		ForecastDisplay.setPadding(new Insets(0,0,0,0));
		presenter = new WeatherPresenter();
		LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> hourlyWeatherData = presenter.PresentHourlyData(location);
		
//		Changing the index of the array below will change the day and data we will loop through below. (0-2)
		String firstKey = (String) hourlyWeatherData.keySet().toArray()[checkoutDay];
		LinkedHashMap<String, LinkedHashMap<String, String>> hourlyDataToday = hourlyWeatherData.get(firstKey);
		
//		Now looping through the hours of the day chosen above.
		hourlyDataToday.keySet().forEach(hour -> {
			
			LinkedHashMap<String, String> data = new LinkedHashMap<String, String>(hourlyDataToday.get(hour));
			HBox oneSingleHourDataToDisplay = new HBox();
			
//			Now i will get the data and store it in preferred the objects, usually type Text:
			Text time = new Text(data.get("time")); time.setStyle("-fx-font: 18 arial;");
			Text rain = new Text(data.get("precipitation")+"mm"); rain.setStyle("-fx-font: 18 arial;");
			Text temperature = new Text(data.get("temperature")+"�C");
			
//			Is it ok to have this little piece of logic in my code? set temperature to red or blue:
			if ((Double.parseDouble(data.get("temperature"))) >= 0) 
				temperature.setStyle("-fx-fill: red; -fx-font-size: 18px;");
			else temperature.setStyle("-fx-fill: blue; -fx-font-size: 18px;");
			
			Text windSpeed = new Text(data.get("windSpeed")+"ms"); windSpeed.setStyle("-fx-font: 18 arial;");
			Double windDirection = Double.parseDouble(data.get("windDirection"));
			
//			Now i will add the data object to the HBox hour
			oneSingleHourDataToDisplay.getChildren().add(time);
			FileInputStream input;
			
			try { //For displaying weather symbol:
				String path = (String.format("src/main/resources/icons/%s.PNG", data.get("symbol")));
				input = new FileInputStream(path);
				Image image = new Image(input);
				ImageView symbol = new ImageView(image);
				symbol.setFitHeight(13);
				symbol.setFitWidth(13);
				symbol.setScaleX(3.6);
				symbol.setScaleY(3.6);
				
				oneSingleHourDataToDisplay.getChildren().add(symbol);
				} catch (FileNotFoundException e) {e.printStackTrace();}
			
			oneSingleHourDataToDisplay.getChildren().add(temperature);
			oneSingleHourDataToDisplay.getChildren().add(rain);
			oneSingleHourDataToDisplay.getChildren().add(windSpeed);
			
			try { //For displaying weather arrow:
				String path = (("src/main/resources/icons/arrow3.PNG"));
				input = new FileInputStream(path);
				Image image = new Image(input);
				ImageView symbol = new ImageView(image);
				symbol.setFitHeight(10);
				symbol.setFitWidth(10);
				symbol.setScaleX(3.3);
				symbol.setScaleY(3.3);
				symbol.setRotate(windDirection+90);
				oneSingleHourDataToDisplay.getChildren().add(symbol);
				} catch (FileNotFoundException e) {e.printStackTrace();}
			
//			Now i will set the layout of the HBox with the content of this hour, 
//			before adding it to the row in the VBox: Forecast Display:
			
			oneSingleHourDataToDisplay.setPadding(new Insets(10,10,10,10));
			oneSingleHourDataToDisplay.setSpacing(65);
			ForecastDisplay.getChildren().add(oneSingleHourDataToDisplay);
		});
	}
	
	private void DisplayPeriodicWeatherData(String location) {
		DisplayInformationText("Feature coming soon!", "", "black");
	}
	
	@FXML 
	private void ToggleHourlyPeriodic(ActionEvent event) throws IOException{
		if (this.location != null) {
			this.SetDay(0);
			if(toggleButton.isSelected() == true) {
				DisplayPeriodicWeatherData(this.location);
			}
			else DisplayHourlyWeatherData(this.location);
		}
	}
	
	
	@FXML
	private void PreviousDay(ActionEvent event) throws IOException {
		if (this.location != null) {
			SetDay(-1);
			if (toggleButton.isSelected() != true)
				this.DisplayHourlyWeatherData(location);
			else
				this.DisplayPeriodicWeatherData(location);
		}
	}
	@FXML
	private void NextDay(ActionEvent event) throws IOException {
		if (this.location != null) {
			SetDay(1);
			if (toggleButton.isSelected() != true)
				this.DisplayHourlyWeatherData(location);
			else
				this.DisplayPeriodicWeatherData(location);
		}	
	}

	private void SetDay(int diff) {
		this.nextDayButton.setDisable(false);
		this.previousDayButton.setDisable(false);
		if (diff < 0) {
			if (this.checkoutDay != 0) {
				this.checkoutDay --;
			}
		}
		if (diff > 0) {
			if(toggleButton.isSelected() == false) {
				this.checkoutDay ++;
				if (this.checkoutDay >= 2) {
					this.checkoutDay = 2;
					this.nextDayButton.setDisable(true);
				}
			}
			
			if(toggleButton.isSelected() == true) {
				this.checkoutDay ++;
				if(checkoutDay >= 6) {
					this.checkoutDay = 6;
					this.nextDayButton.setDisable(true);
				}
			}
		}
		if (diff == 0) {
			this.checkoutDay = 0;
		}
		if (this.checkoutDay == 0) this.previousDayButton.setDisable(true);
		
		SetDateText();
	}
	private void SetDateText() {
		LocalDateTime date = LocalDateTime.now().plusDays(checkoutDay);
		checkoutDateText.setText(date.toString().substring(0, 10)+"- -"+date.getDayOfWeek().toString());
	}
	
	
	@FXML
	private void RemoveThisLocation(ActionEvent event)  {
		System.out.println("Removed: "+this.location);
		if (this.location != null) {
			AddPreferenceButton.setDisable(true);
			weatherExtractor.RemoveLocation(this.location);
			removeThisLocationButton.setText("");
			removeThisLocationButton.setDisable(true);
			LoadLocationsToGUI();
			ForecastDisplay.getChildren().clear();
			LoadBigSun();
			this.location = null;
			if(toggleButton.isSelected())
				toggleButton.setSelected(false);
		}
	}
	
	@FXML
	private void openAddLocationMenu() {
		addLocationMenu.setHideOnClick(false);
	}
	
	@FXML
	private void openAddPreferenceMenu() {
		AddPreferenceMenu.setHideOnClick(false);
		updatePreferenceMenu(location);
	}

	
	@FXML
	private void AddLocation(ActionEvent event) {
		String location = locationName.getText();
		String lat_coord = locationLatitude.getText();
		String long_coord = locationLongitude.getText();
		
		try {
			this.weatherExtractor.AddLocation(location, lat_coord, long_coord);
			addLocationMenu.setHideOnClick(true);
			this.LoadLocationsToGUI();
			locationName.clear();
			locationLatitude.clear();
			locationLongitude.clear();
		}
		catch(IllegalArgumentException error) {
			addLocationErrorMessage.setText(error.getMessage());
		}
	}

	
	@FXML
	private void addPreferenceButtonClicked(ActionEvent event) {
		LinkedHashMap<String, String> preferences = getPreferenceValues();
		try {
			this.preferenceSetter.setPreferences(location, preferences);
			AddPreferenceMenu.setHideOnClick(true);
			this.LoadLocationsToGUI();
		}
		catch(IllegalArgumentException error) {
			addPreferenceErrorMessage.setText(error.getMessage());
		}
	}
	
	
	
	private LinkedHashMap<String, String> getPreferenceValues() {
		LinkedHashMap<String, String> preferences = new LinkedHashMap<String, String>();
		addPreferenceErrorMessage.setText("");
		preferences.put("clearSky",  ((Boolean)this.clearSky.isSelected()).toString());
		preferences.put("someCloud",  ((Boolean)this.someCloud.isSelected()).toString());
		preferences.put("allCloud",  ((Boolean)this.allCloud.isSelected()).toString());
		preferences.put("someRain",  ((Boolean)this.someRain.isSelected()).toString());
		preferences.put("allRain", ((Boolean)this.allRain.isSelected()).toString());
		
		preferences.put("fromWind",  this.fromWind.getText());
		preferences.put("toWind",  this.toWind.getText());
		
		preferences.put("fromTemp",  this.fromTemp.getText());
		preferences.put("toTemp",  this.toTemp.getText());
		
		preferences.put("fromTime",  this.fromTime.getText());
		preferences.put("toTime",  this.toTime.getText());
		
		preferences.put("N",  ((Boolean)this.N.isSelected()).toString());
		preferences.put("NE",  ((Boolean)this.NE.isSelected()).toString());
		preferences.put("E",  ((Boolean)this.E.isSelected()).toString());
		preferences.put("SE",  ((Boolean)this.SE.isSelected()).toString());
		preferences.put("S",  ((Boolean)this.S.isSelected()).toString());
		preferences.put("SW",  ((Boolean)this.SW.isSelected()).toString());
		preferences.put("W",  ((Boolean)this.W.isSelected()).toString());
		preferences.put("NW",  ((Boolean)this.NW.isSelected()).toString());
		
		return preferences;
	}
	private void updatePreferenceMenu(String location){
		addPreferenceErrorMessage.setText("Apply the following preferences to "+ location);
		JSONObject preferences = (JSONObject) preferenceVerifier.getJSONFileData().get(location);
		if (preferences != null) {
			this.clearSky.selectedProperty().set(Boolean.parseBoolean(preferences.get("clearSky").toString()));
			this.someCloud.selectedProperty().set(Boolean.parseBoolean(preferences.get("someCloud").toString()));
			this.allCloud.selectedProperty().set(Boolean.parseBoolean(preferences.get("allCloud").toString()));
			this.someRain.selectedProperty().set(Boolean.parseBoolean(preferences.get("someRain").toString()));
			this.allRain.selectedProperty().set(Boolean.parseBoolean(preferences.get("allRain").toString()));
			
			this.fromWind.setText(preferences.get("fromWind").toString());
			this.toWind.setText(preferences.get("toWind").toString());
			
			this.fromTemp.setText(preferences.get("fromTemp").toString());
			this.toTemp.setText(preferences.get("toTemp").toString());
			
			this.fromTime.setText(preferences.get("fromTime").toString());
			this.toTime.setText(preferences.get("toTime").toString());
			
			this.N.selectedProperty().set(Boolean.parseBoolean(preferences.get("N").toString()));
			this.NE.selectedProperty().set(Boolean.parseBoolean(preferences.get("NE").toString()));
			this.E.selectedProperty().set(Boolean.parseBoolean(preferences.get("E").toString()));
			this.SE.selectedProperty().set(Boolean.parseBoolean(preferences.get("SE").toString()));
			this.S.selectedProperty().set(Boolean.parseBoolean(preferences.get("S").toString()));
			this.SW.selectedProperty().set(Boolean.parseBoolean(preferences.get("SW").toString()));
			this.W.selectedProperty().set(Boolean.parseBoolean(preferences.get("W").toString()));
			this.NW.selectedProperty().set(Boolean.parseBoolean(preferences.get("NW").toString()));
		}
	}
}



















